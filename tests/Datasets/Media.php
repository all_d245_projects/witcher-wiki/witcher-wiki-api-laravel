<?php

use App\Models\Eloquent\Media\MediaUpload;

dataset('media', function () {
    yield fn () => MediaUpload::factory()->count(20)->create();
});
