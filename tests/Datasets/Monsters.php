<?php

use App\Models\Eloquent\Witcher\Monster;

dataset('monsters', function () {
    yield fn () => Monster::factory()->count(30)->create();
});

dataset('monsters-with-variations', function () {
    yield fn () => Monster::factory()->count(20)->hasVariations(3)->create();
});

dataset('monsters-with-susceptibilities', function () {
    yield fn () => Monster::factory()->count(20)->hasSusceptibilities(3)->create();
});
