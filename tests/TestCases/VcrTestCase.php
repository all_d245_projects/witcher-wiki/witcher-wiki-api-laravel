<?php

namespace Tests\TestCases;

use allejo\VCR\VCRCleaner;

class VcrTestCase extends LaravelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @param array $options
     * @return void
     */
    public function enableWitcherFandomVcrCleaner(array $options = []): void
    {
        $defaultOptions = [
            'request' => [
                'ignoreHeaders' => [
                    'Authorization'
                ]
            ]
        ];

        $mergedOptions = array_merge($defaultOptions, $options);

        VCRCleaner::enable($mergedOptions);
    }
}
