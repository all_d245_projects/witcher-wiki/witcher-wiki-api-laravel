<?php

use Faker\Generator;
use Illuminate\Container\Container;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

test('[GET] - /api/v1/media - returns found media', function () {
    $response = actingAsWithScopes(['media:read'])->getJson('/api/v1/media');

    $response->assertStatus(200);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpSuccessResponse()
        ->data->each()->toBeMedia()
        ->data->toBeGreaterThan(5)
        ->statusCode->toBe(200);
})->with('media')->group('auth', 'ok');


test('[GET] - /api/v1/media - returns no found media', function () {
    $response = actingAsWithScopes(['media:read'])->getJson('/api/v1/media');

    $response->assertStatus(200);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpSuccessResponse()
        ->data->toHaveCount(0)
        ->statusCode->toBe(200);
})->group('auth', 'ok');

test('[POST] - /api/v1/media - Uploads media', function () {
    Storage::fake('media');

    $data = [
        'media' => UploadedFile::fake()->create('image.png', 2000, 'image/png'),
    ];

    $response = actingAsWithScopes(['media:create'])
        ->post(
            "/api/v1/media",
            $data
        );

    $response->assertStatus(201);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpSuccessResponse()
        ->data->toBeMedia()
        ->data->imageName->toBe('image')
        ->data->fileName->toBe('image.png')
        ->statusCode->toBe(201);
})->group('auth', 'ok')->skip();

test('[POST] - /api/v1/media - throws 400 on invalid post data', function () {
    $response = actingAsWithScopes(['media:create'])
        ->post(
            "/api/v1/media",
        );

    $response->assertStatus(400);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpErrorResponse()
        ->errors->toHaveKey('media')
        ->statusCode->toBe(400);
})->group('auth', 'ko');
