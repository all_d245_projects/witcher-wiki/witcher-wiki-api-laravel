<?php

test('[GET] - /media/path/to/some/file returns 403 on missing token', function () {
    $response = $this->get("/media/path/to/some/file");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('media', 'ko', 'no-auth');

test('[GET] - /media/path/to/some/file returns 403 on invalid scopes', function () {
    $user = getAuth0JWTUser();

    $response = actingAs($user)->get("/media/path/to/some/file");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('media', 'ko', 'no-auth');
