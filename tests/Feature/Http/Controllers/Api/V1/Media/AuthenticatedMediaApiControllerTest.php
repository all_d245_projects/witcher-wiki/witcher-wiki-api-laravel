<?php

use App\Models\Eloquent\Witcher\Monster;
use Faker\Generator;
use Illuminate\Container\Container;

//test('[GET] - /api/v1/media - returns found media', function () {
//    $response = actingAsWithScopes(['meida:read'])->getJson('/api/v1/media');
//
//    $response->assertStatus(200);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpSuccessResponse()
//        ->data->each()->toBeMonster()
//        ->data->toBeGreaterThan(5)
//        ->statusCode->toBe(200);
//})->with('media')->group('auth', 'ok');

//test('[GET] - /api/v1/monsters - returns found monsters with variations', function () {
//
//    $response = actingAsWithScopes(['monster:read'])->getJson('/api/v1/monsters');
//
//    $response->assertStatus(200);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpSuccessResponse()
//        ->data->each()->toBeMonster()
//        ->data->each()->variations->data->each()->toBeMonsterVariations()
//        ->data->toBeGreaterThan(5)
//        ->statusCode->toBe(200);
//})->skip()->with('monsters-with-variations')->group('auth', 'ok');
//
//test('[GET] - /api/v1/monsters/{monsterId} - returns a single monster by id', function () {
//    $monster = Monster::first();
//    $id = $monster->id;
//
//    $response = actingAsWithScopes(['monster:read'])->getJson("/api/v1/monsters/{$id}");
//
//    $response->assertStatus(200);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpSuccessResponse()
//        ->data->toBeMonster()
//        ->data->id->toBe($id)
//        ->statusCode->toBe(200);
//})->with('monsters')->group('auth', 'ok');
//
//test('[GET] - /api/v1/monsters - returns no found monsters', function () {
//    $response = actingAsWithScopes(['monster:read'])->getJson('/api/v1/monsters');
//
//    $response->assertStatus(200);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpSuccessResponse()
//        ->data->toHaveCount(0)
//        ->statusCode->toBe(200);
//})->group('auth', 'ok');
//
//test('[POST] - /api/v1/monsters - creates a monster', function () {
//    $data = [
//        'data' => [
//            'name' => 'Dog',
//            'description' => 'This is a dog',
//            'class' => "beasts",
//        ]
//    ];
//
//    $response = actingAsWithScopes(['monster:create'])
//        ->postJson(
//            "/api/v1/monsters",
//            $data
//        );
//
//    $response->assertStatus(201);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpSuccessResponse()
//        ->data->toBeMonster()
//        ->data->name->toBe($data['data']['name'])
//        ->data->description->toBe($data['data']['description'])
//        ->data->class->toBe($data['data']['class'])
//        ->statusCode->toBe(201);
//})->group('auth', 'ok');
//
//test('[POST] - /api/v1/monsters - throws 400 on invalid post data', function () {
//    $faker = Container::getInstance()->make(Generator::class);
//
//    $invalidName['data'] = getValidWitcherMonster();
//    $invalidName['data']['name'] = $faker->text(500);
//
//    $invalidClass['data'] = getValidWitcherMonster();
//    $invalidClass['data']['class'] = "invalid-class";
//
//    $badRequests = [
//        'name' => $invalidName,
//        'class' => $invalidClass
//    ];
//
//    foreach ($badRequests as $key => $requestData) {
//        $response = actingAsWithScopes(['monster:create'])
//            ->postJson(
//                "/api/v1/monsters",
//                $requestData
//            );
//
//        $response->assertStatus(400);
//
//        $content = $response->getContent();
//
//        expect($content)->toBeJson();
//
//        expect($content)->json()
//            ->toBeHttpErrorResponse()
//            ->errors->toHaveKey('data.' . $key)
//            ->statusCode->toBe(400);
//    }
//})->group('auth', 'ko');
//
//test('[POST] - /api/v1/monsters - throws 400 on missing post keys', function () {
//    $invalidData = [];
//
//    $response = actingAsWithScopes(['monster:create'])
//        ->postJson(
//            "/api/v1/monsters",
//            $invalidData
//        );
//
//    $response->assertStatus(400);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpErrorResponse()
//        ->errors->toHaveKey('data')
//        ->errors->toHaveKey('data.name')
//        ->errors->toHaveKey('data.class')
//        ->statusCode->toBe(400);
//
//    $invalidName['data'] = getValidWitcherMonster();
//    unset($invalidName['data']['name']);
//
//    $invalidClass['data'] = getValidWitcherMonster();
//    unset($invalidClass['data']['class']);
//
//    $badRequests = [
//        'name' => $invalidName,
//        'class' => $invalidClass
//    ];
//
//    foreach ($badRequests as $key => $request) {
//        $response = actingAsWithScopes(['monster:create'])
//            ->postJson(
//                "/api/v1/monsters",
//                $request
//            );
//
//        $response->assertStatus(400);
//
//        $content = $response->getContent();
//
//        expect($content)->toBeJson();
//
//        expect($content)->json()
//            ->toBeHttpErrorResponse()
//            ->errors->toHaveKey('data.' . $key)
//            ->statusCode->toBe(400);
//    }
//})->group('auth', 'ko');
//
//test('[PATCH] - /api/v1/monsters/{monsterId} = updates a monster', function () {
//    $monster = Monster::first();
//    $monsterId = $monster->id;
//
//    $dataName['data']['name'] = 'updated name';
//    $dataDescription['data']['description'] = 'updated description';
//    $dataclass['data']['class'] = Monster::HYBRIDS_CLASS;
//
//    $updates = [
//      'name' => $dataName,
//      'description' => $dataDescription,
//      'class' => $dataclass
//    ];
//
//    foreach ($updates as $key => $requestData) {
//        $response = actingAsWithScopes(['monster:update'])
//            ->patchJson(
//                "/api/v1/monsters/{$monsterId}",
//                $requestData
//            );
//
//        $response->assertStatus(200);
//
//        $content = $response->getContent();
//
//        expect($content)->toBeJson();
//
//        expect($content)->json()
//            ->toBeHttpSuccessResponse()
//            ->data->toBeMonster()
//            ->data->{$key}->toBe($requestData['data'][$key])
//            ->statusCode->toBe(200);
//    }
//})->with('monsters')->group('auth', 'ok');
//
//test('[PATCH] - /api/v1/monsters/{monsterId} - throws 400 on invalid patch data', function () {
//    $faker = Container::getInstance()->make(Generator::class);
//
//    $monster = Monster::first();
//    $monsterId = $monster->id;
//
//    $invalidName['data'] = getValidWitcherMonster();
//    $invalidName['data']['name'] = $faker->text(500);
//
//    $invalidClass['data'] = getValidWitcherMonster();
//    $invalidClass['data']['class'] = "invalid-class";
//
//    $badRequests = [
//        'name' => $invalidName,
//        'class' => $invalidClass
//    ];
//
//    foreach ($badRequests as $key => $requestData) {
//        $response = actingAsWithScopes(['monster:update'])
//            ->patchJson(
//                "/api/v1/monsters/{$monsterId}",
//                $requestData
//            );
//
//        $response->assertStatus(400);
//
//        $content = $response->getContent();
//
//        expect($content)->toBeJson();
//
//        expect($content)->json()
//            ->toBeHttpErrorResponse()
//            ->errors->toHaveKey('data.' . $key)
//            ->statusCode->toBe(400);
//    }
//})->with('monsters')->group('auth', 'ok');
//
//test('[PATCH] - /api/v1/monsters/{monsterId} - throws 400 on missing patch keys', function () {
//    $monster = Monster::first();
//    $monsterId = $monster->id;
//
//    $invalidData = [];
//
//    $response = actingAsWithScopes(['monster:update'])
//        ->patchJson(
//            "/api/v1/monsters/{$monsterId}",
//            $invalidData
//        );
//
//    $response->assertStatus(400);
//
//    $content = $response->getContent();
//
//    expect($content)->toBeJson();
//
//    expect($content)->json()
//        ->toBeHttpErrorResponse()
//        ->errors->toHaveKey('data')
//        ->statusCode->toBe(400);
//})->with('monsters')->group('auth', 'ok');
