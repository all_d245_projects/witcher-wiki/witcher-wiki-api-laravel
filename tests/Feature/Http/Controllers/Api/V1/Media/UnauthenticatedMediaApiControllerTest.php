<?php

test('[GET] - /api/v1/media returns 403 on missing token', function () {
    $response = $this->getJson("/api/v1/media");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('ko', 'no-auth');

test('[GET] - /api/v1/media returns 403 on invalid scopes', function () {
    $user = getAuth0JWTUser();

    $response = actingAs($user)->getJson("/api/v1/media");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('ko', 'no-auth');


test('[POST] - /api/v1/media returns 403 on missing token', function () {
    $response = $this->postJson("/api/v1/media", []);

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('ko', 'no-auth');
