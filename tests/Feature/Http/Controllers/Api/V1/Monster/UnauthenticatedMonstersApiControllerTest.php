<?php

use App\Models\Eloquent\Witcher\Monster;

test('[GET] - /api/v1/monsters returns 403 on missing token', function () {
    $response = $this->getJson("/api/v1/monsters");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('ko', 'no-auth');

test('[GET] - /api/v1/monsters returns 403 on invalid scopes', function () {
    $user = getAuth0JWTUser();

    $response = actingAs($user)->getJson("/api/v1/monsters");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('ko', 'no-auth');

test('[GET] - /api/v1/monsters/{monsterId} returns 403 on missing token', function () {
    $monster = Monster::first();
    $id = $monster->id;

    $response = $this->getJson("/api/v1/monsters/{$id}");

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->with('monsters')->group('ko', 'no-auth');

test('[POST] - /api/v1/monsters returns 403 on missing token', function () {
    $response = $this->postJson("/api/v1/monsters", []);

    $response->assertStatus(403);

    $content = $response->getContent();

    expect($content)->toBeJson();

    expect($content)->json()
        ->toBeHttpAuthErrorResponse();
})->group('ko', 'no-auth');
