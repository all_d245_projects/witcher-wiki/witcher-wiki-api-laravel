<?php

use App\Models\Eloquent\Witcher\Monster;
use App\Models\Eloquent\Witcher\Item;

test('saving monster with variation', function () {
    $monster = new Monster();
    $monster->name = 'variation 1';
    $monster->save();

    $monste2 = new Monster();
    $monste2->name = 'variation 2';

    $monster->variations()->save($monste2);

    $foundMonster = Monster::firstWhere('name', 'variation 1');

    expect($foundMonster->variations()->first()->name)->toEqual('variation 2');
});

test('saving monster with susceptibilities', function () {
    $monster = new Monster();
    $monster->name = 'variation 1';
    $monster->save();

    $item = new Item();
    $item->name = 'bomb 1';
    $item->type = Item::BOMB_TYPE;

    $monster->susceptibilities()->save($item);

    $foundMonster = Monster::firstWhere('name', 'variation 1');

    expect($foundMonster->susceptibilities()->first()->name)->toEqual('bomb 1');
});
