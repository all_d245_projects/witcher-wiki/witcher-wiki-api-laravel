<?php

use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCases\LaravelTestCase;
use Auth0\Laravel\Model\Stateless\User as Auth0JWTUser;
use Auth0\Laravel\StateInstance;

/*
|--------------------------------------------------------------------------
| Test Case
|--------------------------------------------------------------------------
|
| The closure you provide to your test functions is always bound to a specific PHPUnit test
| case class. By default, that class is "PHPUnit\Framework\TestCase". Of course, you may
| need to change it using the "uses()" function to bind a different classes or traits.
|
*/

uses(LaravelTestCase::class)->in('Feature', 'Unit');

uses(RefreshDatabase::class)->in('Feature');

/*
|--------------------------------------------------------------------------
| Test Groups
|--------------------------------------------------------------------------
|
*/
uses()->group('integration')->in('Feature');

uses()->group('unit')->in('Unit');

uses()->group('witcher-monster')->in('Feature/Http/Controllers/Api/V1/Monster');

uses()->group('media')->in('Feature/Http/Controllers/Api/V1/Media');

/*
|--------------------------------------------------------------------------
| Expectations
|--------------------------------------------------------------------------
|
| When you're writing tests, you often need to check that values meet certain conditions. The
| "expect()" function gives you access to a set of "expectations" methods that you can use
| to assert different things. Of course, you may extend the Expectation API at any time.
|
*/

expect()->extend('toBeOne', function () {
    return $this->toBe(1);
});

expect()->extend('toBeHttpSuccessResponse', function () {
    return
        $this->toHaveKeys(['data', 'statusCode', 'message']);
});

expect()->extend('toBeHttpErrorResponse', function () {
    return
        $this->toHaveKeys(['data', 'statusCode', 'message', 'errors']);
});

expect()->extend('toBeHttpAuthErrorResponse', function () {
    return
        $this
        ->toHaveKeys(['data', 'statusCode', 'message'])
        ->statusCode->toBeIn([401, 403])
        ->data->toHaveCount(0);
});

expect()->extend('toBeHttpGenericErrorResponse', function () {
    return
        $this
        ->toHaveKeys(['data', 'statusCode', 'message'])
        ->data->toHaveCount(0);
});

expect()->extend('toBeMonster', function () {
    return
        $this->toHaveKeys([
            "id",
            "createdAt",
            "updatedAt",
            "name",
            "class",
            "description",
            "variations",
            "susceptibilities",
            "primaryImage",
            "images"
        ]);
});

expect()->extend('toBeMonsterVariations', function () {
    return
        $this->toHaveKeys([
            "id",
            "createdAt",
            "updatedAt",
            "name",
            "class",
            "description",
            "susceptibilities",
            "primaryImage",
            "images"
        ]);
});

expect()->extend('toBeMedia', function () {
    return
        $this->toHaveKeys([
            "id",
            "name",
            "fileName",
            "size",
            "type",
            "mime",
            "urls",
            "urls.original",
            "urls.responsive"
        ]);
});

/*
|--------------------------------------------------------------------------
| Functions
|--------------------------------------------------------------------------
|
| While Pest is very powerful out-of-the-box, you may have some testing code specific to your
| project that you don't want to repeat in every file. Here you can also expose helpers as
| global functions to help you to reduce the number of lines of code in your test files.
|
*/

function something()
{
    // ..
}

function getValidWitcherMonster(): array
{
    return [
        "name" => "Draconid",
        "description" => "Some monster called Draconid.",
        "class" => "beasts"
    ];
}

/**
 * Set the currently logged in user for the application.
 *
 * @param Auth0JWTUser $user
 * @param array $scopes
 * @param string $driver
 * @return mixed
 * @throws BindingResolutionException
 */
function actingAs(Auth0JWTUser $user, array $scopes = [], string $driver = 'auth0'): mixed
{
    $actingAs =  test()->actingAs($user, $driver);
    $state = app()->make(StateInstance::class);
    $state->setAccessTokenScope($scopes);

    return $actingAs;
}

/**
 * @param array $scopes
 * @return Auth0JWTUser
 */
function getAuth0JWTUser(array $scopes = []): Auth0JWTUser
{
    return new Auth0JWTUser([
        'scope' => implode(' ', $scopes),
    ]);
}

/**
 * @param array $scopes
 * @param string $driver
 * @return mixed
 * @throws BindingResolutionException
 */
function actingAsWithScopes(array $scopes = [], string $driver = 'auth0'): mixed
{
    $user = getAuth0JWTUser($scopes);
    return actingAs($user, $scopes, $driver);
}
