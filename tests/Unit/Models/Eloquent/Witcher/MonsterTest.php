<?php

use App\Models\Eloquent\Witcher\Monster;

it('sets the default attributes of a witcher monster', function () {
    $monster = new Monster();
    expect($monster->class)->toBe(Monster::UNDEFINED_CLASS);
})->group('unit', 'witcher-monster', 'ok');
