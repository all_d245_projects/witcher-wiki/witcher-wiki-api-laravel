<?php

use App\Models\Eloquent\Witcher\Item;

it('sets the default attributes of a witcher item', function () {
    $item = new Item();
    expect($item->type)->toBe(Item::UNDEFINED_TYPE);
})->group('unit', 'witcher-item', 'ok');
