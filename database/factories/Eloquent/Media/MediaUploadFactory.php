<?php

namespace Database\Factories\Eloquent\Media;

use App\Models\Eloquent\Media\MediaUpload;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class MediaUploadFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = MediaUpload::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [];
    }

    public function configure()
    {
        return $this->afterCreating(function (MediaUpload $mediaUpload) {
            if (app()->environment('testing')) {
                Storage::fake('media');
                $mediaUpload
                    ->addMedia(UploadedFile::fake()->create('image.png', 17, 'image/png'))
                    ->toMediaCollection();
            }
        });
    }
}
