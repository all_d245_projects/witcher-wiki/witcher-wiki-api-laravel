<?php

namespace Database\Factories\Eloquent\Witcher;

use App\Models\Eloquent\Witcher\Item;
use Illuminate\Database\Eloquent\Factories\Factory;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'type' => $this->faker->randomElement(Item::ITEM_TYPES),
            'name' => $this->faker->text(100),
            'description' => $this->faker->paragraph(3),
        ];
    }
}
