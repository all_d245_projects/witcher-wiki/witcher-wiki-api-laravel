<?php

namespace Database\Factories\Eloquent\Witcher;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Database\Eloquent\Factories\Factory;

class MonsterFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Monster::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'class' => $this->faker->randomElement(Monster::MONSTER_CLASSES),
            'name' => $this->faker->text(100),
            'description' => $this->faker->paragraph(3),
            'primary_image' => '/path/to/some/file-1',
            'images' => [
                '/path/to/some/file-1',
                '/path/to/some/file-2',
                '/path/to/some/file-3',
            ],
        ];
    }
}
