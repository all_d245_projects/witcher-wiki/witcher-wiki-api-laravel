<?php

namespace Database\Seeders;

use App\Models\Eloquent\Witcher\Item;
use Illuminate\Database\Seeder;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::factory()
            ->count(10)
            ->create();
    }
}
