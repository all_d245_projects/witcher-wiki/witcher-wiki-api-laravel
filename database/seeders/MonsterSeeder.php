<?php

namespace Database\Seeders;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Database\Seeder;

class MonsterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Monster::factory()
            ->count(10)
            ->create();

        Monster::factory()
            ->count(5)
            ->hasVariations(3)
            ->create();

        Monster::factory()
            ->count(5)
            ->hasSusceptibilities(3)
            ->create();
    }
}
