<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Eloquent\Witcher\Monster;

class CreateWitcherMonsterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('witcher_monsters', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            // Data
            $table->string('name', 250);

            $table->enum('class', Monster::MONSTER_CLASSES)
                ->default(Monster::UNDEFINED_CLASS);

            $table->text('description')
                ->nullable();

            $table->text('primary_image')
                ->nullable();

            $table->json('images')
                ->nullable();

            // Indices
            $table->unique('name');
            $table->index('class');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('witcher_monsters');
    }
}
