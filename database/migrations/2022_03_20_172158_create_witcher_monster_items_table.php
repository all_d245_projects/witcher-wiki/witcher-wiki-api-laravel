<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWitcherMonsterItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('witcher_monster_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            // Relations
            $table->foreignId('monster_id')
                ->nullable()
                ->constrained('witcher_monsters');

            // Relations
            $table->foreignId('item_id')
                ->nullable()
                ->constrained('witcher_items');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('witcher_monster_items');
    }
}
