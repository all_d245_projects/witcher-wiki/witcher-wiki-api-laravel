<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use App\Models\Eloquent\Witcher\Item;

class CreateWitcherItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('witcher_items', function (Blueprint $table) {
            $table->id();
            $table->timestamps();

            // Data
            $table->string('name', 250);

            $table->enum('type', Item::ITEM_TYPES)
                ->default(Item::UNDEFINED_TYPE);

            $table->text('description')
                ->nullable();

            // Indices
            $table->unique('name');
            $table->index('type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('witcher_items');
    }
}
