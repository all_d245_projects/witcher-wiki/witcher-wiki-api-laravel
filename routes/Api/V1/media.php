<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\V1\MediaApiController;

/*
|--------------------------------------------------------------------------
| API V1 Media Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => '/media', 'middleware' => 'auth0.authorize'], function () {

    // Media READ
    Route::middleware('auth0.authorize:media:read')->group(function () {
        Route::get('/', [MediaApiController::class, 'index']);
    });

    // Media CREATE
    Route::middleware('auth0.authorize:media:create')->group(function () {
        Route::post('/', [MediaApiController::class, 'create']);
    });
});
