<?php

use App\Http\Controllers\Api\V1\MonsterApiController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API V1 Monster Routes
|--------------------------------------------------------------------------
|
*/

Route::group(['prefix' => '/monsters', 'middleware' => 'auth0.authorize'], function () {

    // Monster READ
    Route::middleware('auth0.authorize:monster:read')->group(function () {
        Route::get('/', [MonsterApiController::class, 'index']);
        Route::get('/{monsterId}', [MonsterApiController::class, 'show']);
    });

    // Monster CREATE
    Route::middleware('auth0.authorize:monster:create')->group(function () {
        Route::post('/', [MonsterApiController::class, 'create']);
    });

    // Monster UPDATE
    Route::middleware('auth0.authorize:monster:update')->group(function () {
        Route::patch('/{monsterId}', [MonsterApiController::class, 'update']);
    });
});
