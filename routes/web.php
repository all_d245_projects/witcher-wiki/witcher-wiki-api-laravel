<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\MediaFetchController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Media
Route::group(['prefix' => '/media', 'middleware' => 'auth0.authorize'], function () {

    // Media fetch
    Route::middleware('auth0.authorize:media:fetch')->group(function () {
        Route::get('/{pathToFile}', [MediaFetchController::class, 'fetch'])->where('pathToFile', '.*');
    });
});
