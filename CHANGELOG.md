# [1.0.0-dev.22](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.21...v1.0.0-dev.22) (2022-06-19)


### Features

* configure media url [[#36](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/36)] ([27b88b8](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/27b88b8d0308e2dc7c426b1187b58942988dc77c))

# [1.0.0-dev.21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.20...v1.0.0-dev.21) (2022-06-18)


### Features

* implement image upload [[#20](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/20)] ([adcf61b](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/adcf61bae29747edf903fcd620a0af60337a5de1))

# [1.0.0-dev.20](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.19...v1.0.0-dev.20) (2022-06-18)


### Features

* implement image upload [[#20](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/20)] ([07650ad](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/07650ada28df753d3b69f01ee2ccf1091710120d))

# [1.0.0-dev.19](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.18...v1.0.0-dev.19) (2022-03-29)


### Bug Fixes

* make firewall global [[#34](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/34)] ([51ed99b](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/51ed99bff8f7669f8b95a3fa32daf7d16033fe19))

# [1.0.0-dev.18](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.17...v1.0.0-dev.18) (2022-03-29)


### Features

* add firewall whitelist on heroku dev [[#27](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/27)] ([6a59715](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/6a5971556f25a5fc915032483afb525bd09d8f61))

# [1.0.0-dev.17](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.16...v1.0.0-dev.17) (2022-03-29)


### Bug Fixes

* telescope view error [[#32](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/32)] ([84eec4f](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/84eec4f5bae73beb1abb9110f5df9aaa633af46b))

# [1.0.0-dev.16](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.15...v1.0.0-dev.16) (2022-03-29)


### Bug Fixes

* fix Telescope for production environment [[#31](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/31)] ([49754db](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/49754db379a07a15ebfa0d29b9b24b61d8f5d6b3))

# [1.0.0-dev.15](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.14...v1.0.0-dev.15) (2022-03-27)


### Bug Fixes

* fix telescope for development env [[#29](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/29)] ([73b7e4a](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/73b7e4ae969e2423c7b83a3b5ac49985cdc802cc))

# [1.0.0-dev.14](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.13...v1.0.0-dev.14) (2022-03-27)

# [1.0.0-dev.13](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.12...v1.0.0-dev.13) (2022-03-27)

# [1.0.0-dev.12](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.11...v1.0.0-dev.12) (2022-03-27)

# [1.0.0-dev.11](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.10...v1.0.0-dev.11) (2022-03-27)

# [1.0.0-dev.10](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.9...v1.0.0-dev.10) (2022-03-26)

# [1.0.0-dev.9](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.8...v1.0.0-dev.9) (2022-03-26)

# [1.0.0-dev.8](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.7...v1.0.0-dev.8) (2022-03-26)

# [1.0.0-dev.7](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.6...v1.0.0-dev.7) (2022-03-26)

# [1.0.0-dev.6](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.5...v1.0.0-dev.6) (2022-03-26)

# [1.0.0-dev.5](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.4...v1.0.0-dev.5) (2022-03-26)


### Features

* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([e22f860](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/e22f860a11af31a2616359b1a2232ecf11bc7eb8))

# [1.0.0-dev.4](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.3...v1.0.0-dev.4) (2022-03-26)


### Features

* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([0160093](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/0160093595e0ccbb07f8e17dd3c001563e132636))

# [1.0.0-dev.3](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.2...v1.0.0-dev.3) (2022-03-26)


### Features

* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([e9a080d](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/e9a080d2c84699597f260d5f771df04095d36c01))

# [1.0.0-dev.2](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/compare/v1.0.0-dev.1...v1.0.0-dev.2) (2022-03-26)


### Features

* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([c0b3259](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/c0b325948f02d96324d77f5c93f8c689aa63ea00))

# 1.0.0-dev.1 (2022-03-26)


### Bug Fixes

* remove APP_KEY from example env [[#11](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/11)] ([1f94152](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/1f941528dda44759543bb4b5476a6283c61b39cf))


### Features

* build api client to consume witcher wiki api [[#3](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/3)] ([2c75ef0](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/2c75ef0cd982f8f66f76a76620595ca459966f3a))
* create api routes [[#13](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/13)] ([899483e](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/899483ebbfb8cbe0ff9949e722daccaa0d733282))
* create crud routes for monster [[#18](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/18)] ([ff53603](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/ff53603a9c42c9540f891c3de67ea85d2946c0b9))
* create custom gitlab pages html [[#9](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/9)] ([bc8da1e](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/bc8da1e2fd9fc16e4d1985b5612a60950b0f3e73))
* create monster database model [[#16](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/16)] ([d7bdeb1](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/d7bdeb1db00ce6f06f1c3a1dad51bbd5de94471f))
* design & develop models for beasts [[#2](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/2)] ([589598f](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/589598f7e7767f4b1b34ff64b1ed8c7d35bed885))
* implement flow to retrieve a beast from a search query [[#4](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/4)] ([63dfbca](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/63dfbca515629f19f3ea7e0bbc8b0e6ccaa47ff2))
* initialize project [[#1](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/1)] ([fcf34a3](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/fcf34a3f6781724999f3d099c588c5612902a48e))
* integrate auth0 as aaas provider [[#19](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/19)] ([0bf233c](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/0bf233cf43f9223230fb5f8f0f2c06e0aa67ece6))
* major project change (beastiary to wiki) [[#15](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/15)] ([3d86379](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/3d863791796d433b55abad57fbfa7bc99a117ab5))
* migrate to spattie Laravel ignition [[#22](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/22)] ([60ea1db](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/60ea1db513e429e8d7ad7090726bdffca0ee787b))
* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([f27adf6](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/f27adf6ede626a5aaf5067d9df4ccbf763757249))
* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([f3f68d6](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/f3f68d6dff9cee7f78c01105022d502196254199))
* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([32d59d8](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/32d59d830af91b4f1787bdf78ec1afce39145f46))
* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([d8202ca](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/d8202ca69d11f63a74164178b9aee5bf2e1839f8))
* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([6f09adf](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/6f09adfae9cd19a4762019b0b79d0a8b6b0ffb86))
* prepare first development deploy [[#21](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/issues/21)] ([4c8a8c9](https://gitlab.com/all_d245_projects/witcher-wiki/witcher-wiki-api-laravel/commit/4c8a8c9098be8d6c705c76d712481b115ce34ab4))
