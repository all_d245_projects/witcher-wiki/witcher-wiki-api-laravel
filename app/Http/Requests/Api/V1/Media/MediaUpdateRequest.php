<?php

namespace App\Http\Requests\Api\V1\Media;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MediaUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'mediaId' => 'uuid',
            'media' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ];
    }

    /**
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all(mixed $keys = null): array
    {
        $data = parent::all($keys);
        $data['mediaId'] = $this->route('mediaId');
        return $data;
    }
}
