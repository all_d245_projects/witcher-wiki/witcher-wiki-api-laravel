<?php

namespace App\Http\Requests\Api\V1\Media;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MediaCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'media' => 'required|image|mimes:jpeg,png,jpg|max:2048'
        ];
    }
}
