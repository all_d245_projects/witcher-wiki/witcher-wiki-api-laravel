<?php

namespace App\Http\Requests\Api\V1\Monster;

use Illuminate\Foundation\Http\FormRequest;

class MonsterIndexRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [];
    }
}
