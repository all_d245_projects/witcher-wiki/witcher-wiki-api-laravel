<?php

namespace App\Http\Requests\Api\V1\Monster;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MonsterUpdateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'monsterId' => 'integer|gt:0',

            // data
            'data' => 'required',
            'data.name' => 'filled|string|max:250',
            'data.class' => [
                'filled',
                Rule::in(Monster::MONSTER_CLASSES)
            ],
        ];
    }

    /**
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all(mixed $keys = null): array
    {
        $data = parent::all($keys);
        $data['monsterId'] = $this->route('monsterId');
        return $data;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $class = sprintf(
            'Invalid :attribute. Must be one of (%s)',
            implode(',', Monster::MONSTER_CLASSES)
        );

        return [
            'data.class.in' => $class,
        ];
    }
}
