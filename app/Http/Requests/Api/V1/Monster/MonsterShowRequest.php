<?php

namespace App\Http\Requests\Api\V1\Monster;

use Illuminate\Foundation\Http\FormRequest;

class MonsterShowRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'monsterId' => 'integer|gt:0'
        ];
    }

    /**
     * @param  array|mixed|null  $keys
     * @return array
     */
    public function all(mixed $keys = null): array
    {
        $data = parent::all($keys);
        $data['monsterId'] = $this->route('monsterId');
        return $data;
    }
}
