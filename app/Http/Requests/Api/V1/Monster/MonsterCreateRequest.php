<?php

namespace App\Http\Requests\Api\V1\Monster;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MonsterCreateRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // data
            'data' => 'required',
            'data.name' => 'required|string|max:250',
            'data.class' => [
                'required',
                Rule::in(Monster::MONSTER_CLASSES)
            ]
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        $class = sprintf(
            'Invalid :attribute. Must be one of (%s)',
            implode(',', Monster::MONSTER_CLASSES)
        );


        return [
            'data.class.in' => $class,
        ];
    }
}
