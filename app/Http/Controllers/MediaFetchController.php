<?php

namespace App\Http\Controllers;

use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Illuminate\Support\Str;

class MediaFetchController
{
    /**
     * @param Request $request
     * @return BinaryFileResponse
     */
    public function fetch(Request $request): BinaryFileResponse
    {
        try {
            $filePath = storage_path($this->getFilePath($request->getPathInfo()));
            return response()->file($filePath);
        } catch (FileNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND, 'Media not be found');
        }
    }

    /**
     * @param string $requestPath
     * @return string
     */
    private function getFilePath(string $requestPath): string
    {
        return
            config('witcher-wiki.media_library_storage_directory_path') .
            Str::replaceFirst('/media', '', $requestPath);
    }
}
