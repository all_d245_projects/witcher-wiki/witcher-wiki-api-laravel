<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Requests\Api\V1\Monster\MonsterCreateRequest;
use App\Http\Requests\Api\V1\Monster\MonsterIndexRequest;
use App\Http\Requests\Api\V1\Monster\MonsterShowRequest;
use App\Http\Requests\Api\V1\Monster\MonsterUpdateRequest;
use App\Services\MonsterService;
use App\Transformers\V1\MonsterTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;

class MonsterApiController extends AbstractApiController
{
    /**
     * @var MonsterService
     */
    private MonsterService $monsterService;

    /**
     * @var MonsterTransformer
     */
    private MonsterTransformer $monsterTransformer;

    /**
     * @param MonsterService $monsterService
     * @param MonsterTransformer $monsterTransformer
     */
    public function __construct(MonsterService $monsterService, MonsterTransformer $monsterTransformer)
    {
        $this->monsterService = $monsterService;

        $this->monsterTransformer = $monsterTransformer;
    }

    /**
     * @OA\Get(
     *      path="/monsters",
     *      operationId="getMonsters",
     *      tags={"monsters"},
     *      summary="Get collection of monsters",
     *      description="Retrieves collection of monsters",
     *      @OA\Response(
     *          response=200,
     *          description="Monsters retrieved|No Monsters",
     *          @OA\JsonContent(ref="#/components/schemas/ApiResponse"),
     *       )
     * )
     *
     * @param MonsterIndexRequest $request
     * @return JsonResponse
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function index(MonsterIndexRequest $request): JsonResponse
    {
        $monsterPaginator = $this->monsterService->getMonsterPaginator();

        $monsters = $monsterPaginator->getCollection();

        $monsterCollection = $this->createCollectionResource($monsters, $this->monsterTransformer);

        $monsterCollection->setPaginator(new IlluminatePaginatorAdapter($monsterPaginator));

        $status = Response::HTTP_OK;

        $message = $monsterCollection->getData()->isEmpty()
            ? 'No monsters.'
            : 'Monsters retrieved.';

        return $this->sendJsonResponse($monsterCollection, $status, $message);
    }

    /**
     * @OA\Get(
     *     path="/monsters/{monsterId}",
     *     tags={"monsters"},
     *     summary="Find monster by ID",
     *     description="Retrieves a single monster",
     *     operationId="getMonsterById",
     *     @OA\Parameter(
     *         name="monsterId",
     *         in="path",
     *         description="ID of monster to return",
     *         required=true,
     *         @OA\Schema(
     *             type="integer",
     *             format="int64"
     *         )
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Monster retrieved",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse"),
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Invalid monsterId supplied",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse"),
     *     ),
     *     @OA\Response(
     *         response=404,
     *         description="Monster with id could not be found",
     *         @OA\JsonContent(ref="#/components/schemas/ApiResponse"),
     *     ),
     * )
     *
     * @param MonsterShowRequest $request
     * @return JsonResponse
     */
    public function show(MonsterShowRequest $request): JsonResponse
    {
        $monster = $this->monsterService->findMonsterById($request->monsterId);

        $monsterResource = $this->createItemResource($monster, $this->monsterTransformer);

        return $this->sendJsonResponse($monsterResource, Response::HTTP_OK, 'Monster retrieved.');
    }

    /**
     * @param MonsterUpdateRequest $request
     * @return JsonResponse
     */
    public function update(MonsterUpdateRequest $request): JsonResponse
    {
        $data = $request->validated();

        $id = $data['monsterId'];

        $monsterData = $data['data'];

        $updatedMonster = $this->monsterService->updateMonsterByMonsterId($id, $monsterData);

        $monsterResource = $this->createItemResource($updatedMonster, $this->monsterTransformer);

        return $this->sendJsonResponse($monsterResource, Response::HTTP_OK, 'Monster Updated');
    }

    /**
     * @param MonsterCreateRequest $request
     * @return JsonResponse
     */
    public function create(MonsterCreateRequest $request): JsonResponse
    {
        $data = $request->validated();

        $monsterData = $data['data'];

        $createdMonster = $this->monsterService->createMonster($monsterData);

        $monsterResource = $this->createItemResource($createdMonster, $this->monsterTransformer);

        return $this->sendJsonResponse($monsterResource, Response::HTTP_CREATED, 'Monster Created');
    }
}
