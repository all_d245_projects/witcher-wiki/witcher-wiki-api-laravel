<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Api\AbstractApiController;
use App\Http\Requests\Api\V1\Media\MediaCreateRequest;
use App\Http\Requests\Api\V1\Media\MediaIndexRequest;
use App\Http\Requests\Api\V1\Monster\MonsterUpdateRequest;
use App\Services\MediaService;
use App\Transformers\V1\MediaTransformer;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Http\JsonResponse;

class MediaApiController extends AbstractApiController
{
    /**
     * @var MediaService
     */
    private MediaService $mediaService;

    /**
     * @var MediaTransformer
     */
    private MediaTransformer $mediaTransformer;

    /**
     * @param MediaService $mediaService
     * @param MediaTransformer $mediaTransformer
     */
    public function __construct(MediaService $mediaService, MediaTransformer $mediaTransformer)
    {
        $this->mediaService = $mediaService;

        $this->mediaTransformer = $mediaTransformer;
    }

    /**
     * @OA\Get(
     *      path="/media",
     *      operationId="getMedia",
     *      tags={"media"},
     *      summary="Get collection of media files",
     *      description="Retrieves collection of media files",
     *      @OA\Response(
     *          response=200,
     *          description="Media retrieved|No Media",
     *          @OA\JsonContent(ref="#/components/schemas/ApiResponse"),
     *       )
     * )
     *
     * @param MediaIndexRequest $request
     * @return JsonResponse
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function index(MediaIndexRequest $request): JsonResponse
    {
        $paginator = $this->mediaService->getMediaPaginator();

        $medias = $paginator->getCollection();

        $mediaCollection = $this->createCollectionResource($medias, $this->mediaTransformer);

        $mediaCollection->setPaginator(new IlluminatePaginatorAdapter($paginator));

        $message = $mediaCollection->getData()->isEmpty()
            ? 'No media.'
            : 'Media retrieved.';

        return $this->sendJsonResponse($mediaCollection, Response::HTTP_OK, $message);
    }

    /**
     * @param MonsterUpdateRequest $request
     * @return JsonResponse
     */
//    public function update(MonsterUpdateRequest $request): JsonResponse
//    {
//        $data = $request->validated();
//
//        $uuid = $data['mediaId'];
//
//        $mediaData = $data['file'];
//
//        $updatedMedia = $this->mediaService->updateMediaByUuid($uuid, $mediaData);
//
//        $mediaResource = $this->createItemResource($updatedMedia, $this->mediaTransformer);
//
//        return $this->sendJsonResponse($mediaResource, Response::HTTP_OK, 'Media Updated');
//    }

    /**
     * @param MediaCreateRequest $request
     * @return JsonResponse
     */
    public function create(MediaCreateRequest $request): JsonResponse
    {
        $requestData = $request->validated();

        $createdMedia = $this->mediaService->createMediaFromRequest($requestData);

        $mediaResource = $this->createItemResource($createdMedia, $this->mediaTransformer);

        return $this->sendJsonResponse($mediaResource, Response::HTTP_CREATED, 'Media Created');
    }
}
