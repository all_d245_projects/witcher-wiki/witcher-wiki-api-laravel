<?php

namespace App\Http\Controllers\Api;

use App\Traits\HttpResponseTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller;
use Illuminate\Support\Collection;
use Symfony\Component\HttpFoundation\Response;
use League\Fractal\Resource\Collection as ResourceCollection;
use League\Fractal\Resource\Item;
use League\Fractal\TransformerAbstract;

/**
 * @OA\Info(
 *      title="Witcher Wiki API Laravel",
 *      description="Witcher Wiki API Laravel",
 *      @OA\Contact(
 *          email="asaltson@gmail.com"
 *      )
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_LOCAL_V1_HOST,
 *      description="LOCAL V1 API Server"
 * )
 *
 * @OA\Server(
 *      url=L5_SWAGGER_CONST_DEVELOPMENT_V1_HOST,
 *      description="DEVELOPMENT V1 API Server"
 * )
 *
 */
abstract class AbstractApiController extends Controller
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests, HttpResponseTrait;

    public const DEFAULT_RESPONSE_MESSAGES = [
        // 2XX
        Response::HTTP_OK => 'OK',
        Response::HTTP_CREATED => 'CREATED',
        Response::HTTP_NO_CONTENT => 'NO CONTENT',

        // 4XX
        Response::HTTP_BAD_REQUEST => 'BAD REQUEST',
        Response::HTTP_NOT_FOUND => 'NOT FOUND',
        Response::HTTP_UNAUTHORIZED => 'NOT AUTHORIZED',
        Response::HTTP_FORBIDDEN => "FORBIDDEN",

        // 5xx
        Response::HTTP_INTERNAL_SERVER_ERROR => 'SOMETHING WENT WRONG',
    ];

    /**
     * @param Collection $modelCollection
     * @param TransformerAbstract $transformer
     * @return ResourceCollection
     */
    protected function createCollectionResource(
        Collection $modelCollection,
        TransformerAbstract $transformer
    ): ResourceCollection {
        return new ResourceCollection($modelCollection, $transformer);
    }

    /**
     * @param Model $model
     * @param TransformerAbstract $transformer
     * @return Item
     */
    protected function createItemResource(Model $model, TransformerAbstract $transformer): Item
    {
        return new Item($model, $transformer);
    }
}
