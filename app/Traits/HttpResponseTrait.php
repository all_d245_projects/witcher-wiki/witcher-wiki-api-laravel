<?php

namespace App\Traits;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Api\AbstractApiController;
use League\Fractal\Manager;
use League\Fractal\Resource\ResourceInterface;

trait HttpResponseTrait
{
    /**
     * @param integer $statusCode
     * @param string $message
     * @param array $errors
     * @return JsonResponse
     */
    public function sendDefaultJsonResponse(int $statusCode, string $message = '', array $errors = []): JsonResponse
    {
        $data = $this->getData($statusCode, $message, $errors);
        return new JsonResponse($data, $statusCode);
    }

    /**
     * @param ResourceInterface $resource
     * @param int $statusCode
     * @param string $message
     * @param array $errors
     * @return JsonResponse
     */
    public function sendJsonResponse(
        ResourceInterface $resource,
        int $statusCode,
        string $message = '',
        array $errors = []
    ): JsonResponse {
        $data = $this->getData($statusCode, $message, $errors, $resource);
        return new JsonResponse($data, $statusCode);
    }

    /**
     * @param integer $statusCode
     * @param string $message
     * @param array $errors
     * @param ResourceInterface|null $resource
     * @return array
     */
    private function getData(
        int $statusCode,
        string $message = '',
        array $errors = [],
        ?ResourceInterface $resource = null
    ): array {
        $additionalData = [
            'statusCode' => $statusCode,
            'message' => $this->getMessage($message, $statusCode)
        ];

        if (!empty($errors)) {
            $additionalData['errors'] = $errors;
        }

        if ($resource) {
            $fractal = new Manager();

            return array_merge($fractal->createData($resource)->toArray(), $additionalData);
        }

        return array_merge(['data' => (object)[]], $additionalData);
    }

    /**
     * @param string $message
     * @param int $statusCode
     * @return string
     */
    private function getMessage(string $message, int $statusCode): string
    {
        $messageResponse = '';

        switch (true) {
            case !empty($message):
                $messageResponse = $message;
                break;

            case array_key_exists($statusCode, AbstractApiController::DEFAULT_RESPONSE_MESSAGES):
                $messageResponse = AbstractApiController::DEFAULT_RESPONSE_MESSAGES[$statusCode];
                break;
        }

        return $messageResponse;
    }
}
