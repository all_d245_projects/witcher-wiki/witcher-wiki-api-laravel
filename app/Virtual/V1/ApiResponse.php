<?php

namespace App\Virtual\V1;

/**
 * @OA\Schema(
 *     title="API Response",
 *     @OA\Xml(
 *         name="API Response"
 *     )
 * )
 * @SuppressWarnings(PHPMD)
 */
class ApiResponse
{
    /**
     * @OA\Property(
     *     title="Data",
     *     description="Data wrapper"
     * )
     *
     * @var array|object
     */
    private $data;

    /**
     * @OA\Property(
     *     title="Status Code",
     *     format="int64",
     * )
     *
     * @var int
     */
    private $statusCode;

    /**
     * @OA\Property(
     *     title="Status Code",
     *     format="string",
     * )
     *
     * @var int
     */
    private $message;
}
