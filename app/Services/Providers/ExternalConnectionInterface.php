<?php

namespace App\Services\Providers;

use Psr\Http\Message\ResponseInterface;

interface ExternalConnectionInterface
{
    /**
     * Request made directly without cache
     */
    public const REQUEST_MODE_ORIGIN = 0;

    /**
     * Request made through the cache
     */
    public const REQUEST_MODE_CACHE = 1;

    /**
     * @param string $method
     * @param string $url
     * @param array $requestOptions
     * @return ResponseInterface
     */
    public function sendRequest(
        string $method,
        string $url,
        array $requestOptions = []
    ): ResponseInterface;

    /**
     * Returns maximum number of connections
     *
     * @return integer
     */
    public function getMaxConnections(): int;

    /**
     * Returns Guzzle connection timeout in seconds
     *
     * @return integer
     */
    public function getTimeout(): int;
}
