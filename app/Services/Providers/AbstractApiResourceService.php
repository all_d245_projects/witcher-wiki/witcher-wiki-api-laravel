<?php

namespace App\Services\Providers;

use App\Exceptions\ExternalProviderTrafficException;
use App\Models\Api\ApiResourceInterface;
use App\Services\CacheService;
use App\Services\DataMapperService;
use App\Models\Api\ApiResponse;
use App\Models\Api\ApiResponseInterface;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Redis;
use Exception;
use Illuminate\Support\Facades\Log;

/**
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 * @SuppressWarnings(PHPMD.LongVariable)
 */
abstract class AbstractApiResourceService
{
    /**
     * Maximum retries permitted
     * @var int
     */
    private const MAX_RETRIES = 5;

    /**
     * Wait time in microseconds to wait before attempting another connection
     * @var int
     */
    private const BASE_WAIT_TIME = 1000000; // 1 second

    /**
     * @var CacheService
     */
    private CacheService $cacheService;

    /**
     * @var DataMapperService
     */
    private DataMapperService $dataMapperService;

    /**
     * @var ExternalConnectionInterface
     */
    private ExternalConnectionInterface $httpClient;

    public function __construct(ExternalConnectionInterface $httpClient)
    {
        $this->cacheService = resolve(CacheService::class);
        $this->dataMapperService = resolve(DataMapperService::class);
        $this->httpClient = $httpClient;
    }

    /**
     * @return ExternalConnectionInterface
     */
    public function getHttpClient(): ExternalConnectionInterface
    {
        return $this->httpClient;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    public function getData(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource
    ): ?ApiResponseInterface {
        return ($apiResource->shouldCacheRequest())
            ? $this->handleRequest($connection, $apiResource, 'GET', ExternalConnectionInterface::REQUEST_MODE_CACHE)
            : $this->handleRequest($connection, $apiResource, 'GET');
    }

    /**
     *
     * @param ApiResponseInterface|null $apiResponse
     * @param int|null $successStatusCodes
     * @return object|null
     */
    public function getDeserializedData(?ApiResponseInterface $apiResponse, int $successStatusCodes = null): ?object
    {
        $result = null;

        if ($apiResponse) {
            switch (true) {
                case $successStatusCodes !== null && $successStatusCodes === $apiResponse->getStatusCode():
                case $this->checkResultSuccess($apiResponse->getResponse()):
                    $result = $apiResponse->getData();
                    break;
            }
        }

        return $result;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @param int $requestMode
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final public function handleRequest(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb,
        int $requestMode = ExternalConnectionInterface::REQUEST_MODE_ORIGIN,
    ): ?ApiResponseInterface {
        return match ($requestMode) {
            ExternalConnectionInterface::REQUEST_MODE_ORIGIN =>
                $this->getFromOrigin($connection, $apiResource, $httpVerb),
            ExternalConnectionInterface::REQUEST_MODE_CACHE =>
                $this->getFromCache($connection, $apiResource, $httpVerb),
            default => throw new Exception(),
        };
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws ExternalProviderTrafficException
     */
    final protected function getFromOrigin(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb
    ): ?ApiResponseInterface {
        try {
            $response = $this->handleRequestThroughRedis($connection, $apiResource, $httpVerb);

            $apiResponse = $this->prepareApiResponse($response, $apiResource);
        } catch (Exception $exception) {
            Log::error(
                sprintf(
                    '[%s][%s] - %s',
                    __CLASS__,
                    __FUNCTION__,
                    $exception->getMessage()
                )
            );
            throw $exception;
        }

        return $apiResponse;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ApiResponseInterface|null
     * @throws Exception
     */
    final protected function getFromCache(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb
    ): ?ApiResponseInterface {
        try {
            $requestCallable = function () use ($connection, $apiResource, $httpVerb) {
                return $this->handleRequestThroughRedis($connection, $apiResource, $httpVerb);
            };

            /** @var ResponseInterface $response */
            $response = $this->cacheService->getApiResponseItemFromCache(
                $apiResource,
                $requestCallable
            );

            $apiResponse = $this->prepareApiResponse($response, $apiResource);
        } catch (Exception $exception) {
            Log::error(
                sprintf(
                    '[%s][%s] - %s',
                    __CLASS__,
                    __FUNCTION__,
                    $exception->getMessage()
                )
            );
            throw $exception;
        }

        return $apiResponse;
    }

    /**
     * @param ResponseInterface $response
     * @param ApiResourceInterface $apiResource
     * @return ApiResponseInterface
     * @throws Exception
     */
    private function prepareApiResponse(
        ResponseInterface $response,
        ApiResourceInterface $apiResource
    ): ApiResponseInterface {
        $apiResponse = new ApiResponse($response);

        if (str_contains($apiResource->getDeserializedClass(), 'Collection')) {
            $apiResponse->setData(new Collection());
        }

        $decodedResponse = $apiResponse->getRawData();

        if (is_array($decodedResponse)) {
            if ($this->checkResultSuccess($response) && $apiResource->getDeserializedClass()) {
                $data = $this->dataMapperService->deserializeAndValidate(
                    json_encode($decodedResponse),
                    $apiResource->getDeserializedClass(),
                    $apiResource->getDeserializationGroups()
                );

                $apiResponse->setData($data);
            }
        }

        return $apiResponse;
    }

    /**
     * @param ExternalConnectionInterface $connection
     * @param ApiResourceInterface $apiResource
     * @param string $httpVerb
     * @return ResponseInterface
     * @throws ExternalProviderTrafficException
     * @throws Exception
     */
    private function handleRequestThroughRedis(
        ExternalConnectionInterface $connection,
        ApiResourceInterface $apiResource,
        string $httpVerb
    ): ResponseInterface {
        $response = [];

        $attempts = array_fill(0, $this->getRequestRetries($apiResource), self::BASE_WAIT_TIME);

        $attemptsKeys = array_keys($attempts);

        $lastAttemptIndex = end($attemptsKeys);

        $url = $apiResource->getUrl();

        foreach ($attempts as $index => $time) {
            try {
                $activeCalls = $this->addAttempt($url);

                $this->checkCallAttemptOverLimit($activeCalls, $connection->getMaxConnections(), $url);

                $response = $connection->sendRequest(
                    $httpVerb,
                    $apiResource->getUrl(),
                    $apiResource->getRequestOptions()
                );
            } catch (ClientException | ServerException $exception) {
                Log::info(
                    sprintf(
                        '[%s][%s] - HTTP Client/Server Error | %s',
                        __CLASS__,
                        __FUNCTION__,
                        $exception->getMessage()
                    )
                );
                $response = $exception->getResponse();
            } catch (Exception $exception) {
                Log::error(
                    sprintf(
                        '[%s][%s] - %s',
                        __CLASS__,
                        __FUNCTION__,
                        $exception->getMessage()
                    )
                );
                throw new ExternalProviderTrafficException(sprintf('Attempt error | %s', $exception->getMessage()));
            }

            Redis::decr($url);

            if ($this->checkResultStopsRetries($response, $apiResource->getRetryStatusExempt())) {
                break;
            }

            if ($index !== $lastAttemptIndex) {
                $rand = random_int(0, 20);
                usleep($time + $rand);

                continue;
            }

            throw new ExternalProviderTrafficException('Retry ended without success response');
        }

        return $response;
    }

    /**
     * @param ApiResourceInterface $apiResource
     * @return integer
     */
    private function getRequestRetries(ApiResourceInterface $apiResource): int
    {
        $requestRetries = self::MAX_RETRIES;

        $maxRetries = $apiResource->getRetries();

        if ($maxRetries > 0 && $maxRetries <= self::MAX_RETRIES) {
            $requestRetries = $maxRetries;
        }

        return $requestRetries;
    }

    /**
     * @param string $url
     * @return int
     */
    private function addAttempt(string $url): int
    {
        $activeCalls = Redis::incr($url);
        Redis::expire($url, 10);

        return $activeCalls;
    }

    /**
     * @param int $activeCalls
     * @param int $connectionMaxConnection
     * @param string $url
     * @throws ExternalProviderTrafficException
     */
    private function checkCallAttemptOverLimit(int $activeCalls, int $connectionMaxConnection, string $url): void
    {
        if ($activeCalls > $connectionMaxConnection) {
            $logMessage = sprintf(
                'Traffic shaper: not allowed url: %s active calls: %s',
                $url,
                $activeCalls
            );
            Redis::decr($url);

            throw new ExternalProviderTrafficException($logMessage);
        }
    }

    /**
     * @param ResponseInterface $response
     * @param array $retryStatusExempt
     * @return bool
     */
    private function checkResultStopsRetries(ResponseInterface $response, array $retryStatusExempt): bool
    {
        if ($this->checkResultSuccess($response)) {
            return true;
        }

        if ($this->checkErrorAbortsExecution($response->getStatusCode(), $retryStatusExempt)) {
            return true;
        }

        return false;
    }

    /**
     * @param ResponseInterface $response
     * @return bool
     */
    private function checkResultSuccess(ResponseInterface $response): bool
    {
        $status = $response->getStatusCode();

        return !($status < Response::HTTP_OK || $status >= Response::HTTP_MULTIPLE_CHOICES);
    }

    /**
     * @param int $responseStatusCode
     * @param array $retryStatusExempt
     * @return bool
     */
    private function checkErrorAbortsExecution(int $responseStatusCode, array $retryStatusExempt): bool
    {
        return match (true) {
            in_array($responseStatusCode, $retryStatusExempt) => true,
            default => false,
        };
    }
}
