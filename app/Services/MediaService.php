<?php

namespace App\Services;

use App\Models\Eloquent\Media\MediaUpload;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Exception;

class MediaService
{
    /**
     * @return LengthAwarePaginator
     */
    public function getMediaPaginator(): LengthAwarePaginator
    {
        return Media::paginate();
    }

    /**
     * @param string $uuid
     * @return Media
     */
    public function findMediaByUuid(string $uuid): Media
    {
        return $this->findMedia($uuid);
    }

    /**
     * @param array $requestData
     * @return Media
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function createMediaFromRequest(array $requestData): Media
    {
        try {
            $mediaUpload = MediaUpload::create();
            $mediaUpload
                ->addMediaFromRequest('media')
                ->withResponsiveImages()
                ->toMediaCollection();

            return $mediaUpload->getFirstMedia();
        } catch (Exception $exception) {
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, 'Media could not be created');
        }
    }

    /**
     * @param string $uuid
     * @return Media
     */
    private function findMedia(string $uuid): Media
    {
        /** @var Media|null $media */
        $media = Media::findByUuid($uuid);

        if (!$media) {
            abort(Response::HTTP_NOT_FOUND, sprintf('Media with uuid %s could not be found', $uuid));
        }

        return $media;
    }
}
