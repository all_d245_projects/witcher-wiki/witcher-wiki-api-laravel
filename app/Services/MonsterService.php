<?php

namespace App\Services;

use App\Models\Eloquent\Witcher\Monster;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Exception;

class MonsterService
{
    /**
     * @return LengthAwarePaginator
     */
    public function getMonsterPaginator(): LengthAwarePaginator
    {
        return Monster::paginate();
    }

    /**
     * @param int $id
     * @return Monster
     */
    public function findMonsterById(int $id): Monster
    {
        try {
            return Monster::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND, sprintf('Monster with id %s could not be found', $id));
        }
    }

    /**
     * @param integer $id
     * @param array $data
     * @return Monster
     */
    public function updateMonsterByMonsterId(int $id, array $data): Monster
    {
        try {
            $monster = Monster::findOrFail($id);

            $monster->update($data);

            return $monster;
        } catch (ModelNotFoundException $exception) {
            abort(Response::HTTP_NOT_FOUND, sprintf('Monster with id %s could not be found', $id));
        } catch (Exception $exception) {
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, 'Monster could not be updated');
        }
    }

    /**
     * @param array $data
     * @return Monster
     */
    public function createMonster(array $data): Monster
    {
        try {
            return Monster::create($data);
        } catch (Exception $exception) {
            abort(Response::HTTP_INTERNAL_SERVER_ERROR, 'Monster could not be created');
        }
    }
}
