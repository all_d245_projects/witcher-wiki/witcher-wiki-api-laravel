<?php

namespace App\Services;

use App\Models\Api\ApiResourceInterface;
use Psr\Http\Message\ResponseInterface;
use Illuminate\Support\Facades\Cache;
use GuzzleHttp\Psr7\Response;
use Illuminate\Support\Facades\Log;
use Exception;

class CacheService
{
    public const API_RESPONSE_CACHE_KEY_PREFIX = 'cache_api';

    /**
     * Cache expiration in seconds
     * @var int
     */
    private int $cacheTtl;

    public function __construct()
    {
        $this->cacheTtl = config('witcher-wiki.cache_ttl');
    }

    /**
     * @param ApiResourceInterface $apiResource
     * @param callable $requestCallable
     * @return ResponseInterface
     * @throws Exception
     */
    public function getApiResponseItemFromCache(
        ApiResourceInterface $apiResource,
        callable $requestCallable
    ): ResponseInterface {
        try {
            $key = self::getRequestCacheKeyFromUrl($apiResource);

            $responseData =  Cache::remember($key, $this->cacheTtl, function () use ($requestCallable) {
                $response = $requestCallable();

                return [
                    'statusCode' => $response->getStatusCode(),
                    'headers' => $response->getHeaders(),
                    'body' => $response->getBody()->getContents(),
                    'protocol' => $response->getProtocolVersion(),
                    'reasonPhrase' => $response->getReasonPhrase(),
                ];
            });

            return new Response(
                $responseData['statusCode'],
                $responseData['headers'],
                $responseData['body'],
                $responseData['protocol'],
                $responseData['reasonPhrase'],
            );
        } catch (Exception $exception) {
            Log::error(
                sprintf(
                    '[%s][%s] - %s',
                    __CLASS__,
                    __FUNCTION__,
                    $exception->getMessage()
                )
            );
            throw $exception;
        }
    }

    /**
     * @param string $url
     * @return string
     */
    public static function getCacheKeyUrlFormat(string $url): string
    {
        $requestString = str_replace('/', '', $url);
        return urlencode($requestString);
    }

    /**
     * @param ApiResourceInterface $apiResource
     * @return string
     */
    public static function getRequestCacheKeyFromUrl(ApiResourceInterface $apiResource): string
    {
        $formattedUrl = self::getCacheKeyUrlFormat($apiResource->getUrl());

        return sprintf(
            '%s_%s_%s',
            self::API_RESPONSE_CACHE_KEY_PREFIX,
            $apiResource->getProvider(),
            $formattedUrl
        );
    }
}
