<?php

namespace App\Exceptions;

use App\Traits\HttpResponseTrait;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Http\Request;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Illuminate\Validation\ValidationException;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

class Handler extends ExceptionHandler
{
    use HttpResponseTrait;
    /**
     * A list of the exception types that are not reported.
     *
     * @var string[]
     */
    protected $dontReport = [];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var string[]
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function register()
    {
        $this->renderable(function (Throwable $exception, Request $request) {
            $message = '';
            $errors = [];

            switch (true) {
                case $exception instanceof HttpException:
                    $message = $exception->getMessage();
                    $statusCode = $exception->getStatusCode();
                    break;
                case $exception instanceof ValidationException:
                    $message = $exception->getMessage();
                    $statusCode = Response::HTTP_BAD_REQUEST;
                    $errors = $exception->validator->errors()->getMessages();
                    break;

                default:
                    $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
            }

            return $this->sendDefaultJsonResponse($statusCode, $message, $errors);
        });
    }
}
