<?php

namespace App\Transformers;

use Illuminate\Database\Eloquent\Model;
use League\Fractal\TransformerAbstract;

abstract class AbstractTransformer extends TransformerAbstract
{
    protected function getTimestampTransformProps(Model $model)
    {
        return [
            /** @phpstan-ignore-next-line */
            'createdAt' => $model->created_at,
            /** @phpstan-ignore-next-line */
            'updatedAt' => $model->updated_at
        ];
    }
}
