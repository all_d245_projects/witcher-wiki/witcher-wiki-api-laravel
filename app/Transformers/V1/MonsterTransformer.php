<?php

namespace App\Transformers\V1;

use App\Models\Eloquent\Witcher\Monster;
use App\Transformers\AbstractTransformer;
use League\Fractal\Resource\Collection as ResourceCollection;

class MonsterTransformer extends AbstractTransformer
{
    /**
     * @var string[]
     */
    protected array $defaultIncludes = [
        'variations'
    ];

    /**
     * @param Monster $monster
     * @return array
     */
    public function transform(Monster $monster): array
    {
        return array_merge(
            [
                'id' => $monster->id,
                'name' => $monster->name,
                'class' => $monster->class,
                'description' => $monster->description,
                'susceptibilities' => $monster->susceptibilities,
                'primaryImage' => $monster->primary_image,
                'images' => $monster->images
            ],
            $this->getTimestampTransformProps($monster)
        );
    }

    /**
     * @param Monster $monster
     * @return ResourceCollection
     */
    public function includeVariations(Monster $monster)
    {
        $variations = $monster->variations;

        return $this->collection($variations, new MonsterVariationTransformer());
    }
}
