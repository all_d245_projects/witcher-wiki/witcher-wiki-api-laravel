<?php

namespace App\Transformers\V1;

use App\Transformers\AbstractTransformer;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class MediaTransformer extends AbstractTransformer
{
    /**
     * @param Media $media
     * @return array
     */
    public function transform(Media $media): array
    {
        return array_merge(
            [
                'id' => $media->uuid,
                'name' => $media->name,
                'fileName' => $media->file_name,
                'size' => $media->size,
                'type' => $media->getTypeAttribute(),
                'mime' => $media->mime_type,
                'urls' => [
                    'original' => $media->getOriginalUrlAttribute(),
                    'responsive' => $media->getResponsiveImageUrls()
                ]
            ],
            $this->getTimestampTransformProps($media)
        );
    }
}
