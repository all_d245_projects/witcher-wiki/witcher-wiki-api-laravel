<?php

namespace App\Transformers\V1;

use App\Models\Eloquent\Witcher\Monster;
use App\Transformers\AbstractTransformer;

class MonsterVariationTransformer extends AbstractTransformer
{
    /**
     * @param Monster $monster
     * @return array
     */
    public function transform(Monster $monster): array
    {
        return array_merge(
            [
                'id' => $monster->id,
                'name' => $monster->name,
                'class' => $monster->class,
                'susceptibilities' => $monster->variations,
                'primaryImage' => $monster->primary_image,
                'images' => $monster->images
            ],
            $this->getTimestampTransformProps($monster)
        );
    }
}
