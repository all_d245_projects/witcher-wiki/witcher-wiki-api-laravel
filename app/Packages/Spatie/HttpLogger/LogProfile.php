<?php

namespace App\Packages\Spatie\HttpLogger;

use Spatie\HttpLogger\LogProfile as BaseLogProfile;
use Illuminate\Http\Request;

class LogProfile implements BaseLogProfile
{
    /**
     * @todo define valid requests to log
     * @param Request $request
     * @return boolean
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function shouldLogRequest(Request $request): bool
    {
        return true;
    }
}
