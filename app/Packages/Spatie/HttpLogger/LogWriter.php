<?php

namespace App\Packages\Spatie\HttpLogger;

use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Spatie\HttpLogger\LogWriter as LogWriterInterface;
use Symfony\Component\HttpFoundation\HeaderBag;

class LogWriter implements LogWriterInterface
{
    /**
     * @param Request $request
     * @return void
     */
    public function logRequest(Request $request): void
    {
        $message = $this->formatMessage($this->getMessage($request));

        Log::channel('app-http')->info($message);
    }

    /**
     * @param Request $request
     * @return array
     */
    public function getMessage(Request $request): array
    {
        $files = (new Collection(iterator_to_array($request->files)))
            ->map([$this, 'flatFiles'])
            ->flatten();

        return [
            'method' => strtoupper($request->getMethod()),
            'uri' => $request->getPathInfo(),
            'body' => $request->except(config('http-logger.except.body')),
            'headers' =>  $this->filterHeaders($request->headers),
            'files' => $files,
        ];
    }

    /**
     * @param array $message
     * @return string
     */
    protected function formatMessage(array $message): string
    {
        $bodyAsJson = json_encode($message['body']);
        $headersAsJson = json_encode($message['headers']);
        $files = $message['files']->implode(',');

        return "{$message['method']} {$message['uri']} - Body: {$bodyAsJson} - Headers: {$headersAsJson} " .
            "- Files: " . $files;
    }

    /**
     * @param mixed $file
     * @return mixed
     */
    public function flatFiles($file): mixed
    {
        if ($file instanceof UploadedFile) {
            return $file->getClientOriginalName();
        }
        if (is_array($file)) {
            return array_map([$this, 'flatFiles'], $file);
        }

        return (string) $file;
    }

    /**
     * @param HeaderBag $headerBag
     * @return array
     */
    private function filterHeaders(HeaderBag $headerBag): array
    {
        $headerBagClone = clone $headerBag;
        foreach (config('http-logger.except.header') as $headerKey) {
            if ($headerBagClone->has($headerKey)) {
                $headerBagClone->set($headerKey, 'XXXXXXXXXXXXXXXXXXXXXXXX');
            }
        }

        return $headerBagClone->all();
    }
}
