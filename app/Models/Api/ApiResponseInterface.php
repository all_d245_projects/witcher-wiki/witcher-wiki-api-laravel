<?php

namespace App\Models\Api;

use Psr\Http\Message\ResponseInterface;

interface ApiResponseInterface
{
    /**
     * Returns deserialized data
     *
     * @return mixed
     */
    public function getData();

    /**
     * Set deserialized data
     *
     * @param mixed $data
     * @return ApiResponseInterface
     */
    public function setData(mixed $data): ApiResponseInterface;

    /**
     * Returns the PSR7 Response
     *
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface;

    /**
     * Returns json encoded body response if possible or response body string
     *
     * @return array|string
     */
    public function getRawData(): array|string;

    /**
     * Returns the headers of the response
     *
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Returns response status code
     *
     * @return integer
     */
    public function getStatusCode(): int;
}
