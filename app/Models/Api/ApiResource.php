<?php

namespace App\Models\Api;

/**
 * @SuppressWarnings(PHPMD.LongVariable)
 */
class ApiResource implements ApiResourceInterface
{
    /**
     * @var string
     */
    private string $provider;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $class;

    /**
     * @var ?string
     */
    private ?string $deserializedClass = null;

    /**
     * @var string
     */
    private string $url;

    /**
     * @var int
     */
    private int $retries = 3;

    /**
     * @var boolean
     */
    private bool $cacheRequests = true;

    /**
     * @var array
     */
    private array $requestOptions = [];

    /**
     * @var array
     */
    private array $header = [];

    /**
     * @var array
     */
    private array $retryStatusExempt = [];

    /**
     * @var array
     */
    private array $deserializationGroups = [];

    /**
     * @param string $provider
     */
    public function __construct(string $provider)
    {
        $this->provider = $provider;
    }

    /**
     * {@inheritDoc}
     */
    public function getProvider(): string
    {
        return $this->provider;
    }

    /**
     * {@inheritDoc}
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return ApiResource
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getModelClass(): string
    {
        return $this->class;
    }

    /**
     * @param string $class
     * @return ApiResource
     */
    public function setClass(string $class): self
    {
        $this->class = $class;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDeserializedClass(): ?string
    {
        return $this->deserializedClass;
    }

    /**
     * @param string $class
     * @param bool $isCollection
     * @return ApiResource
     * @SuppressWarnings(PHPMD.BooleanArgumentFlag)
     */
    public function setDeserializedClass(string $class, bool $isCollection = false): self
    {
        ($isCollection)
            ? $this->deserializedClass = sprintf('Collection<%s>', $class)
            : $this->deserializedClass = $class;

        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getDeserializationGroups(): array
    {
        return $this->deserializationGroups;
    }

    /**
     * {@inheritDoc}
     */
    public function setDeserializationGroups(array $groups): self
    {
        $this->deserializationGroups = $groups;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     * @return ApiResource
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getHeaders(): array
    {
        return $this->header;
    }

    /**
     * @param array $header
     * @return ApiResource
     */
    public function setHeaders(array $header): self
    {
        $this->header = $header;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getFormattedResponse(array $response)
    {
        return $response;
    }

    /**
     * {@inheritDoc}
     */
    public function getRetries(): int
    {
        return $this->retries;
    }

    /**
     * @param integer $retries
     * @return ApiResource
     */
    public function setRetries(int $retries): self
    {
        $this->retries = $retries;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function shouldCacheRequest(): bool
    {
        return $this->cacheRequests;
    }

    /**
     * @param boolean $value
     * @return ApiResource
     */
    public function setCacheRequest(bool $value): self
    {
        $this->cacheRequests = $value;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRequestOptions(): array
    {
        return $this->requestOptions;
    }

    /**
     * @param array $requestOptions
     * @return ApiResource
     */
    public function setRequestOptions(array $requestOptions): self
    {
        $this->requestOptions = $requestOptions;
        return $this;
    }

    /**
     * @param array $retryStatusExempt
     * @return ApiResource
     */
    public function setRetryStatusExempt(array $retryStatusExempt): self
    {
        $this->retryStatusExempt = $retryStatusExempt;
        return $this;
    }

    /**
     * {@inheritDoc}
     */
    public function getRetryStatusExempt(): array
    {
        return (empty($this->retryStatusExempt))
            ? self::RETRY_STATUS_EXEMPT
            : $this->retryStatusExempt;
    }
}
