<?php

namespace App\Models\Api;

use Illuminate\Support\Collection;
use Psr\Http\Message\ResponseInterface;

class ApiResponse implements ApiResponseInterface
{
    /**
     * Deserialized data
     * @var Collection|object|null
     */
    private mixed $data = null;

    /**
     * @var ResponseInterface
     */
    private ResponseInterface $response;

    /**
     * @param ResponseInterface $response
     */
    public function __construct(ResponseInterface $response)
    {
        $this->response = $response;
    }

    /**
     * @return Collection|object|null
     */
    public function getData(): mixed
    {
        return $this->data;
    }

    /**
     * @param Collection|object $data
     * {@inheritDoc}
     */
    public function setData($data): ApiResponse
    {
        $this->data = $data;
        return $this;
    }

    /**
     * @return ResponseInterface
     */
    public function getResponse(): ResponseInterface
    {
        return $this->response;
    }

    /**
     * {@inheritDoc}
     */
    public function getRawData(): array|string
    {
        $body = (string) ($this->response->getBody());

        $rawData = json_decode($body, true);

        return (is_array($rawData))
            ? $rawData
            : $body;
    }

    /**
     * {@inheritDoc}
     */
    public function getHeaders(): array
    {
        return $this->response->getHeaders();
    }

    /**
     * {@inheritDoc}
     */
    public function getStatusCode(): int
    {
        return $this->response->getStatusCode();
    }
}
