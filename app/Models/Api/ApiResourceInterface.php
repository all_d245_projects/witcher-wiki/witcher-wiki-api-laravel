<?php

namespace App\Models\Api;

use Symfony\Component\HttpFoundation\Response;

interface ApiResourceInterface
{
    /**
     * Default array of HTTP status' which are exempt from automatic retry attempts
     */
    public const RETRY_STATUS_EXEMPT = [
        Response::HTTP_BAD_REQUEST,
        Response::HTTP_FORBIDDEN,
        Response::HTTP_UNAUTHORIZED,
    ];

    /**
     * Returns the name of the API provider
     *
     * @return string
     */
    public function getProvider(): string;

    /**
     * Returns the name given to an API resource
     *
     * @return string
     */
    public function getName(): string;

    /**
     * Returns the resource endpoint url set for the request. Ideally should not include the base url
     *
     * @return string
     */
    public function getUrl(): string;

    /**
     * Returns the model class of the endpoint
     *
     * @return string
     */
    public function getModelClass(): string;

    /**
     * Returns the class of which data should be deserialized via JMS Serializer
     *
     * @return ?string
     */
    public function getDeserializedClass(): ?string;

    /**
     * Returns deserialization groups for JMS Serializer
     *
     * @return array
     */
    public function getDeserializationGroups(): array;

    /**
     * Returns the headers of the api resource set for the request
     *
     * @return array
     */
    public function getHeaders(): array;

    /**
     * Returns the intended response from the raw json decoded (array).
     *
     * @param array $response
     * @return mixed
     */
    public function getFormattedResponse(array $response);

    /**
     * Returns the number of request retries
     *
     * @return integer
     */
    public function getRetries(): int;

    /**
     * Checks whether GET requests should be cached
     *
     * @return boolean
     */
    public function shouldCacheRequest(): bool;

    /**
     * Returns an array of HTTP status' which are exempt from automatic retry attempts
     * Default array: self::RETRY_STATUS_EXEMPT
     *
     * @return array
     */
    public function getRetryStatusExempt(): array;

    /**
     * Returns Guzzle request options
     *
     * @return array
     */
    public function getRequestOptions(): array;
}
