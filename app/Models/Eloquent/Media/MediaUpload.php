<?php

namespace App\Models\Eloquent\Media;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class MediaUpload extends Model implements HasMedia
{
    use InteractsWithMedia, HasFactory;
}
