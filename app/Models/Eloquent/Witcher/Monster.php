<?php

namespace App\Models\Eloquent\Witcher;

use App\Models\AbstractModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Monster extends AbstractModel implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    public const BEST_CLASS = 'beasts';
    public const CURSED_ONES_CLASS = 'cursed ones';
    public const DRACONIDS_CLASS = 'draconids';
    public const ELEMENTA_CLASS = 'elementa';
    public const HYBRIDS_CLASS = 'hybrids';
    public const INSECTOIDS_CLASS = 'insectoids';
    public const NECROPHAGES_CLASS = 'necrophages';
    public const ORGOIDS_CLASS = 'ogroids';
    public const RELICTS_CLASS = 'relicts';
    public const SPECTERS_CLASS = 'specters';
    public const VAMPIRES_CLASS = 'vampires';
    public const UNDEFINED_CLASS = 'undefined';

    public const MONSTER_CLASSES = [
        Monster::BEST_CLASS,
        Monster::CURSED_ONES_CLASS,
        Monster::DRACONIDS_CLASS,
        Monster::ELEMENTA_CLASS,
        Monster::HYBRIDS_CLASS,
        Monster::INSECTOIDS_CLASS,
        Monster::NECROPHAGES_CLASS,
        Monster::ORGOIDS_CLASS,
        Monster::RELICTS_CLASS,
        Monster::SPECTERS_CLASS,
        Monster::VAMPIRES_CLASS,
        Monster::UNDEFINED_CLASS
    ];

    /**
     * @var string
     */
    protected $table = 'witcher_monsters';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'class',
        'description',
        'primary_image',
        'images'
    ];

    /**
     * @var string[]
     */
    protected $with = [
        'variations'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var string[]
     */
    protected $casts = [
        'images' => 'array',
    ];

    /**
     * Default values for attributes
     *
     * @var string[]
     */
    protected $attributes = [
        'class' => Monster::UNDEFINED_CLASS,
    ];

    /**
     * @return BelongsToMany
     */
    public function variations(): BelongsToMany
    {
        return $this->belongsToMany(
            Monster::class,
            'witcher_monster_variations',
            'monster_id',
            'monster_variation_id'
        );
    }

    /**
     * @return BelongsToMany
     */
    public function susceptibilities(): BelongsToMany
    {
        return $this->belongsToMany(
            Item::class,
            'witcher_monster_items',
            'monster_id',
            'item_id'
        );
    }
}
