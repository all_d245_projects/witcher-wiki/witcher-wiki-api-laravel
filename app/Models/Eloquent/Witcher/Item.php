<?php

namespace App\Models\Eloquent\Witcher;

use App\Models\AbstractModel;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Item extends AbstractModel
{
    use HasFactory;

    public const OIL_TYPE = 'oil';
    public const SIGN_TYPE = 'sign';
    public const BOMB_TYPE = 'bomb';
    public const UNDEFINED_TYPE = 'undefined';

    public const ITEM_TYPES = [
        Item::OIL_TYPE,
        Item::SIGN_TYPE,
        Item::BOMB_TYPE,
        Item::UNDEFINED_TYPE
    ];

    /**
     * @var string
     */
    protected $table = 'witcher_items';

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'name',
        'type',
        'description'
    ];

    /**
     * Default values for attributes
     *
     * @var string[]
     */
    protected $attributes = [
        'type' => Item::UNDEFINED_TYPE,
    ];
}
