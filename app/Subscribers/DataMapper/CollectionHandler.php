<?php

namespace App\Subscribers\DataMapper;

use Illuminate\Support\Collection;
use JMS\Serializer\DeserializationContext;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Visitor\DeserializationVisitorInterface;
use JMS\Serializer\Visitor\SerializationVisitorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use ArrayObject;

final class CollectionHandler implements SubscribingHandlerInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribingMethods()
    {
        $methods = [];
        $formats = ['json', 'xml', 'yml'];
        $collectionTypes = [
            'Collection',
            'Illuminate\Support\Collection',
        ];

        foreach ($collectionTypes as $type) {
            foreach ($formats as $format) {
                $methods[] = [
                    'direction' => GraphNavigatorInterface::DIRECTION_SERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'serializeCollection',
                ];

                $methods[] = [
                    'direction' => GraphNavigatorInterface::DIRECTION_DESERIALIZATION,
                    'type' => $type,
                    'format' => $format,
                    'method' => 'deserializeCollection',
                ];
            }
        }

        return $methods;
    }

    /**
     * @return ArrayObject
     */
    public function serializeCollection(
        SerializationVisitorInterface $visitor,
        Collection $collection,
        array $type,
        SerializationContext $context
    ): ArrayObject {
        $type['name'] = 'array';

        $context->stopVisiting($collection);

        $exclusionStrategy = $context->getExclusionStrategy();
        if (null !== $exclusionStrategy &&
            $exclusionStrategy->shouldSkipClass(
                // @phpstan-ignore-next-line
                $context->getMetadataFactory()->getMetadataForClass(get_class($collection)),
                $context
            )
        ) {
            $context->startVisiting($collection);

            return $visitor->visitArray([], $type);
        }

        $result = $visitor->visitArray($collection->toArray(), $type);

        $context->startVisiting($collection);

        return $result;
    }

    /**
     * @param mixed $data
     * @SuppressWarnings(PHPMD.UnusedFormalParameter)
     */
    public function deserializeCollection(
        DeserializationVisitorInterface $visitor,
        $data,
        array $type,
        DeserializationContext $context
    ): Collection {
        $type['name'] = 'array';

        return new Collection($visitor->visitArray($data, $type));
    }
}
