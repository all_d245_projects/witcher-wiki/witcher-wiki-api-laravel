const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public');

// Copy Files
mix.copy('resources/other/public/*', 'public');
mix.copy('resources/other/vendor', 'public/vendor');

if (mix.inProduction()) {
    mix.version()
        .sourceMaps();
}
