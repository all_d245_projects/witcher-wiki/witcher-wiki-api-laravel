<?php

return [

    /*
     * The log profile which determines whether a request should be logged.
     * It should implement `LogProfile`.
     */
    'log_profile' => App\Packages\Spatie\HttpLogger\LogProfile::class,

    /*
     * The log writer used to write the request to a log.
     * It should implement `LogWriter`.
     */
    'log_writer' => App\Packages\Spatie\HttpLogger\LogWriter::class,

    /*
    * The log channel used to write the request.
    */
    'log_channel' => env('LOG_CHANNEL', 'stack'),

    /*
     * Filter out body fields which will never be logged.
     */
    'except' => [
        'body' => [
            'password',
            'password_confirmation'
        ],
        'header' => [
            'authorization'
        ]
    ],
];
